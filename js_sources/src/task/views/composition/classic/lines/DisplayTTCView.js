import Mn from 'backbone.marionette';
import FormBehavior from "../../../../../base/behaviors/FormBehavior.js";
import CheckboxWidget from '../../../../../widgets/CheckboxWidget.js';

const DisplayTTCView = Mn.View.extend({
    template: require('./templates/DisplayTTCView.mustache'),
    fields: ['display_ttc'],
    behaviors: [FormBehavior],
    regions: {
        "content": "div",
    },
    childViewTriggers: {
        'change': 'data:modified',
        'finish': 'data:persist'
    },
    onRender(){
       
    }
});
export default DisplayTTCView;
