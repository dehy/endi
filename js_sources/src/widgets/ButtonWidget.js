import {
    getOpt
} from '../tools.js';
import Mn from 'backbone.marionette';
import IconWidget from './IconWidget.js';
import ButtonModel from '../base/models/ButtonModel.js';

const ButtonWidget = Mn.View.extend({
    /*
     * View for a ButtonModel 
     *
     * Usage:
     * 1- With a ButtonModel
     *      let model = new ButtonModel({
     *          label: 'click me', 
     *          icon: 'plus', 
     *          showLabel: false, 
     *          action: 'clickme',
     *          css: 'btn-info'
     *      });
     *      widget = new ButtonWidget({model: model});
     * 
     * 2- Passing directly the options
     *      widget = new ButtonWidget({
     *          label: 'click me', 
     *          icon: 'plus', 
     *          showLabel: false,
     *          css: 'btn-info'
     *       })
     *
     * 
     * Surrounding Tag (directly to the view)
     * ---------------------------------------
     * 
     * You can specify the surrounding tag and its css (mandatory in marionette) by specifying
     * 
     * @param {string} surroudingTagName: The tagName of this view (default: span)
     * @param {string} surroudingCss: The css of this view (default: '')
     * 
     * Label and icon (on the model or directly to the view)
     * ------------------------------------------------------
     * 
     * @param {string} label: label for this button
     * @param {bool} showLabel: Should we show the label (or only the icon)
     * @param {string} icon: Optionnal name of an icon to add to this button
     * 
     * Triggers
     * --------
     * There are two way to specify triggers
     * 
     * 1- specify an 'action' attribute (on the model or directly)
     * 
     * @event action:clicked
     * @property {string} actionName the 'action' string passed as option
     * 
     * 2- specify an 'event' attribute (on the model or directly)
     * 
     * @event the 'event' string passed as option
     *
     * Full example :
     *
     *      let model = new ButtonModel({label: 'click me', icon: 'plus', showLabel: false});
     *      this.showChildView('button-container', new ButtonWidget({model: model}));
     */
    template: require('./templates/ButtonWidget.mustache'),
    tagName: function () {
        return getOpt(this, 'surroundingTagName', 'span');
    },
    className: function () {
        return getOpt(this, 'surroundingCss', '');
    },
    regions: {
        icon: {
            el: 'icon',
            replaceElement: true
        },
    },
    ui: {
        btn: 'button'
    },
    events: {
        'click @ui.btn': 'onButtonClicked'
    },
    initialize(options) {
        if (!('model' in options)) {
            this.model = new ButtonModel({
                label: getOpt(this, 'label', ''),
                icon: getOpt(this, 'icon', 'question-circle'),
                showLabel: getOpt(this, 'showLabel', true),
            });
            const event = getOpt(this, 'event', false);
            if (event) {
                this.model.set('event', event)
            }
            const action = getOpt(this, 'action', false);
            if (action) {
                this.model.set('action', action)
            }
        }
    },
    onButtonClicked(event) {
        if (this.model.has('action')) {
            let actionName = this.model.get('action');
            this.triggerMethod('action:clicked', actionName);
        } else if (this.model.has('event')) {
            this.triggerMethod(this.model.get('event'));
        } else {
            console.error("ButtonWidget : il manque un attribut action ou event au modèle")
        }
    },
    onRender() {
        let icon = this.model.get('icon');
        if (icon !== false) {
            this.showChildView('icon', new IconWidget({
                icon: icon
            }));
        } else {
            this.removeRegion('icon');
        }
    },
    templateContext() {
        let css = getOpt(this, 'css', '');
        if (this.model.get('css')) {
            css += ' ' + this.model.get('css');
        }
        let result = {
            surroundingTagName: getOpt(this, 'surroundingTagName', false),
            css: css
        }
        if (!this.model.has('title')) {
            result['title'] = this.model.get('label');
        }
        return result;
    }
});
export default ButtonWidget;