from endi.views.business.rest_api import BusinessRestView


def test_get_businesses(
    company,
    dbsession,
    get_csrf_request_with_db,
    business,
):
    request = get_csrf_request_with_db()
    request.context = company
    view = BusinessRestView(request)
    result = view.collection_get()
    assert len(result) == 1
    assert result[0].name == business.name


def test_get_business_filters(
    company,
    dbsession,
    get_csrf_request_with_db,
    business,
    project,
):
    request = get_csrf_request_with_db()
    request.context = company
    view = BusinessRestView(request)

    request.GET = {"project_id": project.id}
    assert len(view.collection_get()) == 1

    request.GET = {"project_id": project.id + 1}
    assert len(view.collection_get()) == 0

    request.GET = {"search": "business"}
    assert len(view.collection_get()) == 1

    request.GET = {"search": "NOEXIST"}
    assert len(view.collection_get()) == 0


def test_get_businesses_other_company(
    company2,
    dbsession,
    get_csrf_request_with_db,
    business,
):
    request = get_csrf_request_with_db()
    request.context = company2
    view = BusinessRestView(request)
    result = view.collection_get()
    assert len(result) == 0
