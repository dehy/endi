<%inherit file="${context['main_template'].uri}" />
<%block name='content'>
<div class='row'>
    <% keys = list(datas.keys()) %>
    <% keys.sort() %>
    % for year in keys:
    <% months = datas[year] %>
    <div class='panel panel-default page-block'>
        <h2 class='panel-heading'>
            <a href="#" data-toggle='collapse' data-target='#year_${year}'>
                <span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#folder-open"></use></svg></span>
                ${year}
            </a>
        </h2>
        <div class='panel-body'>
        % if year in current_years:
            <div class="section-content in collapse" id='year_${year}'>
        %else:
            <div class="section-content collapse" id='year_${year}'>
        %endif
                <table class="table_hover">
                    <thead>
                    	<tr>
							<th scope="col" class="col_text">Mois</th>
							<th scope="col" class="col_number">Nombre de fichiers</th>
							<th scope="col" class="col_actions" title="Actions"><span class="screen-reader-text">Actions</span></th>
                    	</tr>
                    </thead>
                    <tbody>
                    <% month_names = list(months.keys()) %>
                    <% month_names.sort(key=lambda m:int(m)) %>
                    % for month in month_names:
                        <% month_datas = months[month] %>
                        <tr>
                            <td class="col_text">${month_datas['label']}</td>
                            <td class="col_number">${month_datas['nbfiles']} fichier(s)</td>
                            <td class="col_actions width_one">
                            	<a href="${month_datas['url']}" class="btn icon only" title="Administrer" aria-label="Administrer">
                            		<svg><use href="${request.static_url('endi:static/icons/endi.svg')}#pen"></use></svg>
                            	</a>
                            </td>
                        </tr>
                    % endfor
                % if not months:
                    <tr><td colspan='3' class="col_text"><em>Aucun document n’est disponible</em></td></tr>
                % endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    % endfor
    % if not keys:
    <div class='panel panel-default page-block'>
        <div class='panel-body'><em>Aucun document n’est disponible</em></div>
    </div>
    % endif
</div>
</%block>
