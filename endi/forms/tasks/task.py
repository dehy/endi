import functools
import logging
import colander
from typing import Optional

from deform import widget
from colanderalchemy import SQLAlchemySchemaNode
from sqlalchemy.orm import load_only

from endi.utils.html import (
    clean_html,
)
from endi.models.tva import (
    Tva,
    Product,
)
from endi.models.task import WorkUnit
from endi.models.task.mentions import TaskMention
from endi.models.task.task import (
    DiscountLine,
    TaskLineGroup,
    TaskLine,
    ALL_STATES,
)

from endi import forms
from endi.forms.custom_types import (
    AmountType,
    QuantityType,
)
from endi.forms.user import get_deferred_user_choice

from endi.models.project.types import BusinessType
from endi.models.project.business import Business
from endi.models.third_party import Customer
from endi.models.project.project import (
    Project,
    ProjectCustomer,
    ProjectBusinessType,
)
from endi.models.project import Phase
from endi.models.services.naming import NamingService

from endi.models.task.insurance import TaskInsuranceOption
from .base import (
    business_type_id_validator,
    task_after_bind,
    get_task_type_from_factory,
)


logger = logging.getLogger(__name__)


def tva_product_validator(node, value):
    """
    Validator checking that tva and product_id matches
    """
    product_id = value.get("product_id")
    if product_id is not None:
        tva_id = value.get("tva_id")
        if tva_id is not None:
            tva = Tva.get(tva_id)
            if product_id not in [p.id for p in tva.products]:
                exc = colander.Invalid(
                    node, "Ce produit ne correspond pas à la TVA configurée"
                )
                exc["product_id"] = (
                    "Le code produit doit correspondre à la                    "
                    " TVA configurée pour cette prestation"
                )
                raise exc


def _customize_discountline_fields(schema):
    """
    Customize DiscountLine colander schema related fields

    :param obj schema: The schema to modify
    """
    customize = functools.partial(forms.customize_field, schema)
    customize("id", widget=widget.HiddenWidget())
    customize("task_id", missing=colander.required)
    customize(
        "description",
        widget=widget.TextAreaWidget(),
        validator=forms.textarea_node_validator,
        preparer=clean_html,
    )
    customize(
        "amount",
        typ=AmountType(5),
        missing=colander.required,
    )
    customize(
        "tva",
        typ=AmountType(2),
        validator=forms.get_deferred_select_validator(Tva, id_key="value"),
        missing=colander.required,
    )

    return schema


def _customize_taskline_fields(schema):
    """
    Customize TaskLine colander schema related fields

    :param obj schema: The schema to modify
    """
    schema.validator = tva_product_validator
    customize = functools.partial(forms.customize_field, schema)
    customize("id", widget=widget.HiddenWidget())
    customize(
        "description",
        widget=widget.TextAreaWidget(),
        validator=forms.textarea_node_validator,
        preparer=clean_html,
    )
    customize("cost", typ=AmountType(5), missing=colander.required)
    customize("quantity", typ=QuantityType(), missing=colander.required)
    customize(
        "unity",
        validator=forms.get_deferred_select_validator(WorkUnit, id_key="label"),
        missing=colander.drop,
    )
    customize(
        "tva",
        typ=AmountType(2),
        validator=forms.get_deferred_select_validator(Tva, id_key="value"),
        missing=colander.required,
    )
    customize(
        "product_id",
        validator=forms.get_deferred_select_validator(Product),
        missing=colander.drop,
    )
    return schema


def _customize_tasklinegroup_fields(schema):
    """
    Customize TaskLineGroup colander schema related fields

    :param obj schema: The schema to modify
    """
    # pré-remplissage de la variable schema de la fonction
    # forms.customize_field
    customize = functools.partial(forms.customize_field, schema)
    customize("id", widget=widget.HiddenWidget())
    customize("task_id", missing=colander.required)
    customize(
        "description",
        widget=widget.TextAreaWidget(),
        preparer=clean_html,
    )
    customize(
        "lines",
        validator=colander.Length(
            min=1,
            min_err="Une prestation au moins doit être incluse",
        ),
    )
    if "lines" in schema:
        child_schema = schema["lines"].children[0]
        _customize_taskline_fields(child_schema)

    return schema


def _customize_task_fields(schema):
    """
    Add Field customization to the task form schema

    :param obj schema: The schema to modify
    """
    customize = functools.partial(forms.customize_field, schema)
    schema.after_bind = task_after_bind
    customize("id", widget=widget.HiddenWidget(), missing=colander.drop)
    if "status":
        customize(
            "status",
            widget=widget.SelectWidget(values=list(zip(ALL_STATES, ALL_STATES))),
            validator=colander.OneOf(ALL_STATES),
        )
    customize(
        "status_comment",
        widget=widget.TextAreaWidget(),
    )
    customize(
        "status_user_id",
        widget=get_deferred_user_choice(),
    )
    customize(
        "business_type_id",
        validator=business_type_id_validator,
        missing=colander.drop,  # We drop it in edit mode
    )
    customize(
        "description",
        widget=widget.TextAreaWidget(),
        validator=forms.textarea_node_validator,
        missing=colander.required,
    )
    customize("date", missing=colander.required)
    for field_name in "ht", "ttc", "tva", "expenses_ht":
        customize(field_name, typ=AmountType(5))

    customize(
        "address",
        widget=widget.TextAreaWidget(),
        validator=forms.textarea_node_validator,
        missing=colander.required,
    )
    customize(
        "workplace",
        widget=widget.TextAreaWidget(),
    )
    customize(
        "payment_conditions",
        widget=widget.TextAreaWidget(),
        validator=forms.textarea_node_validator,
        missing=colander.required,
    )
    customize(
        "insurance_id",
        validator=forms.get_deferred_select_validator(TaskInsuranceOption),
        # TODO : dynamiquement setter le missing
        missing=colander.drop,
    )
    customize(
        "mentions",
        children=forms.get_sequence_child_item(TaskMention),
    )
    customize(
        "line_groups",
        validator=colander.Length(min=1, min_err="Une entrée est requise"),
        missing=colander.required,
    )
    if "line_groups" in schema:
        child_schema = schema["line_groups"].children[0]
        _customize_tasklinegroup_fields(child_schema)

    if "discount_lines" in schema:
        child_schema = schema["discount_lines"].children[0]
        _customize_discountline_fields(child_schema)
    return schema


def get_add_edit_discountline_schema(includes=None, excludes=None):
    """
    Return add edit schema for DiscountLine edition

    :param tuple includes: field that should be included (if None,
    excludes will be used instead)
    :param tuple excludes: Model attributes that should be excluded for schema
    generation (if None, a default one is provided)

    :rtype: `colanderalchemy.SQLAlchemySchemaNode`
    """
    if includes is not None:
        excludes = None

    schema = SQLAlchemySchemaNode(
        DiscountLine,
        includes=includes,
        excludes=excludes,
    )
    schema = _customize_discountline_fields(schema)
    return schema


def get_add_edit_taskline_schema(includes=None, excludes=None):
    """
    Return add edit schema for TaskLine edition

    :param tuple includes: field that should be included (if None,
    excludes will be used instead)
    :param tuple excludes: Model attributes that should be excluded for schema
    generation (if None, a default one is provided)

    :rtype: `colanderalchemy.SQLAlchemySchemaNode`
    """
    if includes is not None:
        excludes = None

    schema = SQLAlchemySchemaNode(
        TaskLine,
        includes=includes,
        excludes=excludes,
    )
    schema = _customize_taskline_fields(schema)
    return schema


def get_add_edit_tasklinegroup_schema(includes=None, excludes=None):
    """
    Return add edit schema for TaskLineGroup edition

    :param tuple includes: field that should be included (if None,
    excludes will be used instead)
    :param tuple excludes: Model attributes that should be excluded for schema
    generation (if None, a default one is provided)

    :rtype: `colanderalchemy.SQLAlchemySchemaNode`
    """
    if includes is not None:
        excludes = None

    schema = SQLAlchemySchemaNode(TaskLineGroup, includes=includes, excludes=excludes)
    schema = _customize_tasklinegroup_fields(schema)
    return schema


def get_edit_task_schema(
    factory, isadmin=False, includes=None, excludes=None, **kw
) -> SQLAlchemySchemaNode:
    """
    Return a schema for task edition

    :param class factory: The type of task we want to edit
    :param bool isadmin: Are we asking for an admin schema ?
    :param tuple includes: field that should be included (if None,
    excludes will be used instead)
    :param tuple excludes: Model attributes that should be excluded for schema
    generation (if None, a default one is provided)
    :returns: `colanderalchemy.SQLAlchemySchemaNode`
    """
    if includes is not None:
        excludes = None
    elif excludes is None:
        excludes = (
            "id",
            "children",
            "parent",
            "exclude",
            "phase_id",
            "owner_id",
            "company_id",
            "project_id",
            "customer_id",
            "expenses",
            "insurance",
        )
        if not isadmin:
            excludes = excludes + ("status",)

    schema = SQLAlchemySchemaNode(factory, excludes=excludes, includes=includes, **kw)
    schema = _customize_task_fields(schema)
    return schema


task_type_validator = colander.OneOf(("invoice", "cancelinvoice", "estimation"))


def business_type_filter_node(
    name="business_type_id", title="Type d'affaire", default="all"
):
    """
    "Filter by business type" SchemaNode for listings
    """

    return colander.SchemaNode(
        colander.String(),
        name=name,
        title=title,
        widget=deferred_business_type_options,
        validator=deferred_business_type_validator,
        missing=default,
        default=default,
    )


@colander.deferred
def deferred_business_type_options(node, kw):
    business_type_options = get_business_types_option_list()
    return widget.SelectWidget(values=business_type_options)


@colander.deferred
def deferred_business_type_validator(node, kw):
    business_type_options = get_business_types_option_list()
    return colander.OneOf([str(b[0]) for b in business_type_options])


def get_business_types_option_list():
    """
    Return structured option list for business types widget
    """
    options = [
        (business_type.id, business_type.label.title())
        for business_type in BusinessType.query().filter_by(active=True)
    ]
    options.insert(0, ("all", "Tous"))
    return options


@colander.deferred
def deferred_task_add_validator(node, kw):
    dbsession = kw["request"].dbsession

    def task_add_data_integrity(schema: SQLAlchemySchemaNode, appstruct: dict):
        """
        Check that submitted data are compatible with each others

        :raises: colander.Invalid if an error is raised
        """
        project_id = appstruct["project_id"]
        customer_id = appstruct["customer_id"]
        if (
            dbsession.query(ProjectCustomer)
            .filter_by(customer_id=customer_id, project_id=project_id)
            .count()
            == 0
        ):
            raise colander.Invalid(node, "Le client n'est pas associé au projet")

        business_type_id = appstruct.get("business_type_id")
        project = Project.get(project_id)
        if (
            dbsession.query(BusinessType.id)
            .filter_by(id=business_type_id, project_type_id=project.project_type_id)
            .count()
            == 0
        ) and (
            dbsession.query(ProjectBusinessType)
            .filter_by(project_id=project_id, business_type_id=business_type_id)
            .count()
            == 0
        ):
            raise colander.Invalid(
                node, "Cet type d'affaire ne peut être menée dans ce projet"
            )

        phase_id = appstruct.get("phase_id")
        if phase_id:
            if Phase.query().filter_by(id=phase_id, project_id=project_id).count() == 0:
                raise colander.Invalid(
                    node, "Ce sous-dossier n'appartient pas au projet sélectionné"
                )

    return task_add_data_integrity


def get_new_task_name(
    factory, project: Optional[Project] = None, business: Optional[Business] = None
) -> str:
    """Build a default new task name

    :param factory: Estimation / Invoice
    :type factory: class

    :param project: project in which we add a task
    :param business: business in which we add a task (case of progress invoicing)

    :return: the name to use for the new task
    """
    name = ""
    tasktype = get_task_type_from_factory(factory)
    if project is not None:
        method = "get_next_{0}_index".format(tasktype)
        number = getattr(project, method)()

        type_label = NamingService.get_label_for_context(tasktype, project)
        name = f"{type_label} {number}"
    else:
        if tasktype == "estimation":
            name = "Nouveau devis"
        else:
            name = "Nouvelle facture"
    return name


def get_add_task_schema(
    factory,
    request,
    company_id: int,
    customer_id: Optional[int] = None,
    project_id: Optional[int] = None,
    phase_id: Optional[int] = None,
) -> SQLAlchemySchemaNode:
    """
    Build the Task add schema

    NB : must be called "on the fly" in a view's method, not once as class attribute
    """
    dbsession = request.dbsession
    includes = (
        "name",
        "customer_id",
        "project_id",
        "phase_id",
        "business_type_id",
    )
    schema = SQLAlchemySchemaNode(factory, includes=includes)
    customize = functools.partial(forms.customize_field, schema)

    project = None
    business_type_id = None
    # On détermine des valeurs par défaut
    # si le contexte courant est un projet
    if project_id is not None:
        project = Project.get(project_id)
        query = dbsession.query(ProjectCustomer.c.customer_id).filter(
            ProjectCustomer.c.project_id == project_id
        )
        if query.count() == 1:
            # Si on a un seul client, on le sélectionne
            customer_id = query.scalar()

        if project.project_type.default_business_type:
            business_type_id = project.project_type.default_business_type.id

        else:
            btypes = Project.get(project_id).get_all_business_types(request)
            if len(btypes) == 1:
                # SI on a un seul type d'affaire, on le sélectionne
                business_type_id = btypes[0].id

    customize(
        "name",
        title="Nom du document",
        default=get_new_task_name(factory, project),
        description="Ce nom n'apparaît pas dans le document final",
    )

    customize(
        "customer_id",
        title="Client",
        missing=colander.required,
        default=customer_id,
        validator=colander.OneOf(
            [
                c.id
                for c in dbsession.query(Customer)
                .options(load_only("id"))
                .filter(Customer.company_id == company_id)
                .filter(Customer.archived == False)
            ]
        ),
    )
    customize(
        "project_id",
        title="Dossier",
        default=project_id,
        validator=colander.OneOf(
            [
                p.id
                for p in dbsession.query(Project)
                .options(load_only("id"))
                .filter(Project.company_id == company_id)
                .filter(Project.archived == False)
            ]
        ),
    )
    customize(
        "business_type_id",
        title="Type d'affaire",
        validator=business_type_id_validator,
        default=business_type_id,
    )
    customize("phase_id", missing=None, default=phase_id, title="Sous-dossier")
    schema.validator = deferred_task_add_validator
    return schema
