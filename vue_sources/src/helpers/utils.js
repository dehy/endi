import { toRaw, createApp } from 'vue'
import { createPinia } from 'pinia'

/**
 * 
 * @param {*} prefix : String to prefix the unique id
 * @param {*} length : length of the random string
 * @returns A random id string : "prefix<random string of length 'length'>"
 * 
 * uniqueId("firstname-", 4) => firstname-XXXX
 */
export const uniqueId = (prefix="", length=8) => {
    return prefix + Math.ceil(Math.random() * Date.now()).toPrecision(length).toString().replace(/[^a-z0-9_-]/gi, '');   
}


/**
 * Deep clone an object
 * 
 * @param {*} obj : A Js object (Array, Object, ...)
 */
export const clone = (obj) => {
    if (obj === null || typeof (obj) !== 'object' || 'isActiveClone' in obj)
        return obj;

    if (obj instanceof Date)
        var temp = new obj.constructor(); //or new Date(obj);
    else
        var temp = obj.constructor();

    for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            obj['isActiveClone'] = null;
            temp[key] = clone(obj[key]);
            delete obj['isActiveClone'];
        }
    }
    return temp;
}
export const getPathFromString = (strPath) => {
    return strPath.split('.');
}
export const startApp = (MyApplication, tagName="vue-app") => {
    
    const app = createApp(MyApplication)
    // Store management
    const pinia = createPinia()
    app.use(pinia);
    app.mount('#vue-app');
    return app;
}