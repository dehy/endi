endi.plugins.sap.models.task package
====================================

Submodules
----------

endi.plugins.sap.models.task.tasks module
-----------------------------------------

.. automodule:: endi.plugins.sap.models.task.tasks
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.plugins.sap.models.task
   :members:
   :undoc-members:
   :show-inheritance:
