endi.sql\_compute.task package
==============================

Submodules
----------

endi.sql\_compute.task.task module
----------------------------------

.. automodule:: endi.sql_compute.task.task
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.sql_compute.task
   :members:
   :undoc-members:
   :show-inheritance:
