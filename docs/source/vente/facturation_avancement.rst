Facturation à l'avancement
===========================

La facturation à l'avancement est gérée par les modèles présents dans :ref:`endi.models.progress_invoicing` .

Fonctionnement général
-------------------------

Depuis un devis, on génère une affaire que l'on passe en mode "Facturation à l'avancement".

L'affaire (Business) est alors liés à des status d'avancement (Voir les modèles dans `endi.models.progress_invoicing.status`)

À la création d'une facture, on crée un plan d'avancement et des entrées pour chaque chapitre/produit/ouvrage à facturer en se basant sur les statuts existant.

NB : plusieurs devis peuvent être créés dans l'affaire (des statuts sont créés pour chaque nouveau devis "Signé").

Le schéma ci-dessous indique la structure générale

.. image:: ./_static/avancement/diagramme.jpg
  :width: 100%
  :alt: Facturation à l'avancement

